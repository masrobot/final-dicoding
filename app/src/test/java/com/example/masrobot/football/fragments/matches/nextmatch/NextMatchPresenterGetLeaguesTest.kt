package com.example.masrobot.football.fragments.matches.nextmatch

import com.example.masrobot.football.TestContextProvider
import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.api.TheSportDBApi
import com.example.masrobot.football.model.LeagueDetail
import com.example.masrobot.football.model.LeagueResponse
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class NextMatchPresenterGetLeaguesTest {

    @Mock
    private lateinit var view: NextMatchView

    @Mock
    private lateinit var apiRepository: ApiRepository

    @Mock
    private lateinit var gson: Gson

    private lateinit var presenter: NextMatchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = NextMatchPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getLeagues() {
        val leagues: MutableList<LeagueDetail> = mutableListOf()
        val response = LeagueResponse(leagues)

        Mockito.`when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.listAllLeague()),
                LeagueResponse::class.java)).thenReturn(response)

        presenter.getLeagues()
        Mockito.verify(view).showLoading()
        Mockito.verify(view).leagueList(leagues)
        Mockito.verify(view).hideLoading()
    }
}