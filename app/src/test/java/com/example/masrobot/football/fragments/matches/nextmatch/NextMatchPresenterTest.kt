package com.example.masrobot.football.fragments.matches.nextmatch

import com.example.masrobot.football.TestContextProvider
import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.api.TheSportDBApi
import com.example.masrobot.football.model.EventResponse
import com.example.masrobot.football.model.MatchSchedule
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class NextMatchPresenterTest {

    @Mock
    private lateinit var view: NextMatchView

    @Mock
    private lateinit var apiRepository: ApiRepository

    @Mock
    private lateinit var gson: Gson

    private lateinit var presenter: NextMatchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = NextMatchPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getNextMatch() {
        val nextMatch: MutableList<MatchSchedule> = mutableListOf()
        val response = EventResponse(nextMatch)
        val league = "4328"

        `when`(gson.fromJson(apiRepository.doRequest(TheSportDBApi.getNextMatch(league)),
                EventResponse::class.java)).thenReturn(response)

        presenter.getNextMatch(league)
        verify(view).showLoading()
        verify(view).listMatch(nextMatch)
        verify(view).hideLoading()
    }
}