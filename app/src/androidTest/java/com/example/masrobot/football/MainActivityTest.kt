package com.example.masrobot.football

import android.support.test.espresso.Espresso.*
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import com.example.masrobot.football.R.id.*
import com.example.masrobot.football.main.MainActivity
import org.hamcrest.CoreMatchers.anything
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @Rule
    @JvmField var activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainTest() {
        // Select Next Match Spinner and choose one league
        Thread.sleep(5000)
        onView(withId(spinner_next_match)).check(matches(isDisplayed()))
        onView(withId(spinner_next_match)).perform(click())
        Thread.sleep(3000)
        onData(anything()).atPosition(5).perform(click())
        onView(withId(spinner_next_match))
                .check(matches(withSpinnerText("Chinese Super League")))

        // Next Match Recyclerview
        Thread.sleep(4000)
        onView(withId(recycler_view_next_match)).check(matches(isDisplayed()))
        onView(withId(recycler_view_next_match))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(6))
        onView(withId(recycler_view_next_match))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        // Next Match Detail
        Thread.sleep(4000)
        onView(withId(detail_match_layout)).check(matches(isDisplayed()))

        // Add Next Match to Favorite
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Added to favorite")).check(matches(isDisplayed()))
        pressBack()

        // SearchView
        onView(withId(search_match_btn)).check(matches(isDisplayed()))
        onView(withId(search_match_btn)).perform(click())
        onView(withId(search_view)).check(matches(isDisplayed()))
        onView(withId(search_view))
                .perform(typeText("man united"), pressKey(KeyEvent.KEYCODE_BACK))
        Thread.sleep(3000)
        onView(withId(recycler_view_search)).check(matches(isDisplayed()))
        onView(withId(recycler_view_search))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        // Search Result Match Detail
        Thread.sleep(4000)
        onView(withId(detail_match_layout)).check(matches(isDisplayed()))

        // Add Match Search Result to Favorite
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Added to favorite")).check(matches(isDisplayed()))
        pressBack()

        onView(withId(search_view)).check(matches(isDisplayed()))
        onView(withId(search_view))
                .perform(typeText(""), pressKey(KeyEvent.KEYCODE_BACK))
        Thread.sleep(3000)
        onView(withId(recycler_view_search)).check(matches(isDisplayed()))
        pressBack()

        Thread.sleep(2000)
        onView(withId(navigation)).check(matches(isDisplayed()))
        onView(withId(navigation_teams)).perform(click())

        // Teams View
        Thread.sleep(5000)
        onView(withId(spinner_teams)).check(matches(isDisplayed()))
        onView(withId(spinner_teams)).perform(click())
        Thread.sleep(3000)
        onData(anything()).atPosition(5).perform(click())
        onView(withId(spinner_teams))
                .check(matches(withSpinnerText("Chinese Super League")))
        Thread.sleep(3000)
        onView(withId(recycler_view_teams)).check(matches(isDisplayed()))
        onView(withId(recycler_view_teams))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        // Teams Detail
        Thread.sleep(4000)
        onView(withId(detail_team_layout)).check(matches(isDisplayed()))

        // Add Team Favorite
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Added to favorite")).check(matches(isDisplayed()))
        pressBack()

        // Team Search
        onView(withId(search_menu)).check(matches(isDisplayed()))
        onView(withId(search_menu)).perform(click())
        onView(withId(android.support.design.R.id.search_src_text)).check(matches(isDisplayed()))
        onView(withId(android.support.design.R.id.search_src_text))
                .perform(typeText("man united"), pressKey(KeyEvent.KEYCODE_BACK))
        Thread.sleep(3000)
        onView(withId(recycler_view_teams_search)).check(matches(isDisplayed()))
        onView(withId(recycler_view_teams_search))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        // Teams Detail
        Thread.sleep(4000)
        onView(withId(detail_team_layout)).check(matches(isDisplayed()))

        // Add Team Favorite
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Added to favorite")).check(matches(isDisplayed()))
        pressBack()

        onView(withId(android.support.design.R.id.search_src_text)).check(matches(isDisplayed()))
        onView(withId(android.support.design.R.id.search_src_text))
                .perform(typeText(""), pressKey(KeyEvent.KEYCODE_BACK))
        pressBack()
        onView(withId(recycler_view_teams)).check(matches(isDisplayed()))

        Thread.sleep(2000)
        onView(withId(navigation)).check(matches(isDisplayed()))
        onView(withId(navigation_favorites)).perform(click())

        // Favorite View
        Thread.sleep(3000)
        onView(withId(favorite_match_layout)).check(matches(isDisplayed()))
        onView(withText("TEAMS")).perform(click())
        Thread.sleep(3000)
        onView(withId(team_favorite_layout)).check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(recycler_view_favorite_team)).check(matches(isDisplayed()))
        onView(withId(recycler_view_favorite_team))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        // Detail Team Favorite
        Thread.sleep(4000)
        onView(withId(detail_team_layout)).check(matches(isDisplayed()))

        // Remove from Favorite
        onView(withId(add_to_favorite)).check(matches(isDisplayed()))
        onView(withId(add_to_favorite)).perform(click())
        onView(withText("Removed to favorite")).check(matches(isDisplayed()))
        pressBack()

        // Refresh Team Favorite View
        Thread.sleep(2000)
        onView(withId(team_favorite_layout)).check(matches(isDisplayed()))
        onView(withId(swipeRefresh_favorite_team)).check(matches(isDisplayed()))
        onView(withId(swipeRefresh_favorite_team)).perform(swipeDown())
        Thread.sleep(3000)
        onView(withId(team_favorite_layout)).check(matches(isDisplayed()))
    }
}