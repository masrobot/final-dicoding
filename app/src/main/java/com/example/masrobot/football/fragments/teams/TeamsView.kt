package com.example.masrobot.football.fragments.teams

import com.example.masrobot.football.model.LeagueDetail
import com.example.masrobot.football.model.Team

interface TeamsView {
    fun showLoading()
    fun hideLoading()
    fun leagueList(leagueData: List<LeagueDetail>)
    fun showSearch(searchData: List<Team>)
    fun showTeams(teamsData: List<Team>)
}