package com.example.masrobot.football.fragments.favorites


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.masrobot.football.R
import com.example.masrobot.football.fragments.favorites.matchfavorite.MatchFavoriteFragment
import com.example.masrobot.football.fragments.favorites.teamfavorite.TeamFavotiteFragment
import com.example.masrobot.football.fragments.matches.nextmatch.NextMatchFragment
import com.example.masrobot.football.fragments.matches.pastmatch.PastMatchFragment
import com.example.masrobot.football.utils.TabsAdapter
import kotlinx.android.synthetic.main.fragment_favorite.*
import org.jetbrains.anko.support.v4.find

class FavoriteFragment : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val toolbar = find<Toolbar>(R.id.toolbar_favorite)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        val tabsAdapter = TabsAdapter(childFragmentManager)
        tabsAdapter.addFragment(MatchFavoriteFragment(), "MATCHES")
        tabsAdapter.addFragment(TeamFavotiteFragment(), "TEAMS")
        view_pager_favorite.adapter = tabsAdapter
        tabs_favorite.setupWithViewPager(view_pager_favorite)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }


}
