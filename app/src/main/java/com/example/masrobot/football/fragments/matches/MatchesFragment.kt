package com.example.masrobot.football.fragments.matches


import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.*
import com.example.masrobot.football.R
import com.example.masrobot.football.fragments.matches.nextmatch.NextMatchFragment
import com.example.masrobot.football.fragments.matches.pastmatch.PastMatchFragment
import com.example.masrobot.football.searchmatch.SearchMatchActivity
import com.example.masrobot.football.utils.TabsAdapter
import kotlinx.android.synthetic.main.fragment_matches.*
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.startActivity


class MatchesFragment : Fragment() {

    private lateinit var tabsAdapter: TabsAdapter
    private lateinit var toolbar: Toolbar

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar = find(R.id.toolbar)
        toolbar.title = "Football"
        toolbar.setTitleTextColor(Color.WHITE)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        setHasOptionsMenu(true)

        tabsAdapter = TabsAdapter(childFragmentManager)
        tabsAdapter.addFragment(NextMatchFragment(), "NEXT")
        tabsAdapter.addFragment(PastMatchFragment(), "PAST")
        view_pager.adapter = tabsAdapter
        tabs.setupWithViewPager(view_pager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_matches, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.search_match_btn -> {
                startActivity<SearchMatchActivity>()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
