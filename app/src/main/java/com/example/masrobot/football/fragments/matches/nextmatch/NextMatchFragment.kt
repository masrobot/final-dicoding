package com.example.masrobot.football.fragments.matches.nextmatch


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.masrobot.football.R
import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.detailmatch.DetailMatchActivity
import com.example.masrobot.football.fragments.matches.nextmatch.adapter.NextMatchAdapter
import com.example.masrobot.football.model.LeagueDetail
import com.example.masrobot.football.model.MatchSchedule
import com.example.masrobot.football.utils.StringWithTag
import com.example.masrobot.football.utils.invisible
import com.example.masrobot.football.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_next_match.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity

class NextMatchFragment : Fragment(), NextMatchView {

    private lateinit var presenter: NextMatchPresenter
    private var spinnerAdapter: ArrayAdapter<StringWithTag>? = null
    private var nextMatches: MutableList<MatchSchedule> = mutableListOf()
    private lateinit var adapter: NextMatchAdapter
    private var key: String? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val request = ApiRepository()
        val gson = Gson()
        presenter = NextMatchPresenter(this, request, gson)

        // League List
        presenter.getLeagues()

        adapter = NextMatchAdapter(nextMatches) {
            startActivity<DetailMatchActivity>("matchId" to "${it.matchId}",
                    "homeTeamId" to "${it.homeTeamId}",
                    "awayTeamId" to "${it.awayTeamId}")
        }
        recycler_view_next_match.layoutManager = LinearLayoutManager(ctx)
        recycler_view_next_match.adapter = adapter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_next_match, container, false)
    }

    override fun showLoading() {
        progress_next_match.visible()
    }

    override fun hideLoading() {
        progress_next_match.invisible()
    }

    override fun leagueList(leagueData: List<LeagueDetail>) {
        swipeRefresh_next_match.isRefreshing = false

        val item = ArrayList<StringWithTag>()

        leagueData.forEach {
            val key = it.idLeague
            val value = it.strLeague
            item.add(StringWithTag(key as String, value as String))
        }

        spinnerAdapter = ArrayAdapter(ctx, R.layout.support_simple_spinner_dropdown_item,
                item)
        spinner_next_match.adapter = spinnerAdapter
        spinner_next_match.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val swt = parent?.getItemAtPosition(position) as StringWithTag
                key = swt.tag.toString()
                presenter.getNextMatch(key.toString())

                swipeRefresh_next_match.onRefresh {
                    presenter.getNextMatch(key.toString())
                }
            }
        }
    }

    override fun listMatch(matchData: List<MatchSchedule>) {
        swipeRefresh_next_match.isRefreshing = false
        nextMatches.clear()
        nextMatches.addAll(matchData)
        adapter.notifyDataSetChanged()
    }
}
