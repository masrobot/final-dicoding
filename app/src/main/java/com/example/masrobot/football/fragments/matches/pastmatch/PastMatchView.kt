package com.example.masrobot.football.fragments.matches.pastmatch

import com.example.masrobot.football.model.LeagueDetail
import com.example.masrobot.football.model.MatchSchedule

interface PastMatchView {
    fun showLoading()
    fun hideLoading()
    fun leagueList(leagueData: List<LeagueDetail>)
    fun listMatch(matchData: List<MatchSchedule>)
}