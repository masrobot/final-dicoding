package com.example.masrobot.football.detailmatch.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.football.R
import com.example.masrobot.football.model.AwayTeamDetail
import com.example.masrobot.football.model.HomeTeamDetail
import com.example.masrobot.football.model.MatchDetail
import com.example.masrobot.football.utils.*
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find

class DetailMatchAdapter(private val detailMatch: List<MatchDetail>,
                         private val homeTeamDetail: List<HomeTeamDetail>,
                         private val awayTeamDetail: List<AwayTeamDetail>) :
        RecyclerView.Adapter<MatchDetailViewHolder> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchDetailViewHolder =
            MatchDetailViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_detail_match, parent, false))

    override fun getItemCount(): Int = detailMatch.size

    override fun onBindViewHolder(holder: MatchDetailViewHolder, position: Int) {
        holder.bindItem(detailMatch[position], homeTeamDetail[position], awayTeamDetail[position])
    }
}

class MatchDetailViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val dateMatch = view.find<TextView>(R.id.date_match)
    private val timeMatch = view.find<TextView>(R.id.time_match)
    private val homeBadge = view.find<ImageView>(R.id.home_badge)
    private val awayBadge = view.find<ImageView>(R.id.away_badge)
    private val homeScore = view.find<TextView>(R.id.home_score)
    private val awayScore = view.find<TextView>(R.id.away_score)
    private val homeTeam = view.find<TextView>(R.id.home_team)
    private val awayTeam = view.find<TextView>(R.id.away_team)
    private val homeFormation = view.find<TextView>(R.id.home_formation)
    private val awayFormation = view.find<TextView>(R.id.away_formation)
    private val homeGoalsDetail = view.find<TextView>(R.id.home_goals_detail)
    private val awayGoalsDetail = view.find<TextView>(R.id.away_goals_detail)
    private val homeShots = view.find<TextView>(R.id.home_shots)
    private val awayShots = view.find<TextView>(R.id.away_shots)
    private val homeGoalKeeper = view.find<TextView>(R.id.home_goal_keeper)
    private val awayGoalKeeper = view.find<TextView>(R.id.away_goal_keeper)
    private val homeDefence = view.find<TextView>(R.id.home_defence)
    private val awayDefence = view.find<TextView>(R.id.away_defence)
    private val homeMidfield = view.find<TextView>(R.id.home_midfield)
    private val awayMidfield = view.find<TextView>(R.id.away_midfield)
    private val homeForward = view.find<TextView>(R.id.home_forward)
    private val awayForward = view.find<TextView>(R.id.away_forward)
    private val homeSubtitutes = view.find<TextView>(R.id.home_substitutes)
    private val awaySubtitutes = view.find<TextView>(R.id.away_substitutes)

    fun bindItem(detailMatch: MatchDetail,
                 homeTeamDetail: HomeTeamDetail,
                 awayTeamDetail: AwayTeamDetail) {
        val localFormat = toGMTFormat(detailMatch.dateMatch.toString(), detailMatch.timeMatch.toString())
        val dateLocalFormat = dateSimpleString(localFormat)
        val timeLocalFormat = timeSimpleString(localFormat)

        dateMatch.text = dateLocalFormat
        timeMatch.text = timeLocalFormat
        Picasso.with(itemView.context)
                .load(homeTeamDetail.homeTeamBadge)
                .into(homeBadge)
        Picasso.with(itemView.context)
                .load(awayTeamDetail.awayTeamBadge)
                .into(awayBadge)
        homeScore.text = detailMatch.homeScore
        awayScore.text = detailMatch.awayScore
        homeTeam.text = detailMatch.homeTeam
        awayTeam.text = detailMatch.awayTeam
        homeFormation.text = detailMatch.homeFormation
        awayFormation.text = detailMatch.awayFormation
        homeGoalsDetail.text = detailMatch.homeGoalDetails?.replace(";", "\n")
        awayGoalsDetail.text = detailMatch.awayGoalDetails?.replace(";", "\n")
        homeShots.text = detailMatch.homeShots?.replace(";", "\n")
        awayShots.text = detailMatch.awayShots?.replace(";", "\n")
        homeGoalKeeper.text = detailMatch.homeLineupGK?.replace(";", "\n")
        awayGoalKeeper.text = detailMatch.awayLineupGK?.replace(";", "\n")
        homeDefence.text = detailMatch.homeLineupDef?.replace(";", "\n")
        awayDefence.text = detailMatch.awayLineupDef?.replace(";", "\n")
        homeMidfield.text = detailMatch.homeLineupMF?.replace(";", "\n")
        awayMidfield.text = detailMatch.awayLineupMF?.replace(";", "\n")
        homeForward.text = detailMatch.homeLineupST?.replace(";", "\n")
        awayForward.text = detailMatch.awayLineupST?.replace(";", "\n")
        homeSubtitutes.text = detailMatch.homeLineupSubs?.replace(";", "\n")
        awaySubtitutes.text = detailMatch.awayLineupSubs?.replace(";", "\n")
    }
}