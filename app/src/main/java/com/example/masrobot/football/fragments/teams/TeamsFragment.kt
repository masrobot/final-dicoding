package com.example.masrobot.football.fragments.teams


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.masrobot.football.R
import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.detailteam.DetailTeamActivity
import com.example.masrobot.football.fragments.teams.adapter.TeamsAdapter
import com.example.masrobot.football.fragments.teams.adapter.TeamsSearchAdapter
import com.example.masrobot.football.model.LeagueDetail
import com.example.masrobot.football.model.Team
import com.example.masrobot.football.utils.StringWithTag
import com.example.masrobot.football.utils.gone
import com.example.masrobot.football.utils.invisible
import com.example.masrobot.football.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_teams.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity

class TeamsFragment : Fragment(), TeamsView {

    private var searchTeam: MutableList<Team> = mutableListOf()
    private var teams: MutableList<Team> = mutableListOf()
    private lateinit var presenter: TeamsPresenter
    private var spinnerAdapter: ArrayAdapter<StringWithTag>? = null
    private lateinit var adapter: TeamsAdapter
    private lateinit var adapterSearch: TeamsSearchAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val toolbar = find<android.support.v7.widget.Toolbar>(R.id.toolbar_teams)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        setHasOptionsMenu(true)

        val request = ApiRepository()
        val gson = Gson()
        presenter = TeamsPresenter(this, request, gson)
        presenter.getLeagues()

        adapter = TeamsAdapter(teams) {
            startActivity<DetailTeamActivity>(
                    "teamId" to it.teamId,
                    "teamName" to it.teamName,
                    "teamBadge" to it.teamBadge,
                    "teamYear" to it.teamFormedYear,
                    "teamStadium" to it.teamStadium,
                    "teamDesc" to it.teamDescription
            )
        }
        recycler_view_teams.layoutManager = LinearLayoutManager(ctx)
        recycler_view_teams.adapter = adapter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_teams, container, false)
    }

    override fun showLoading() {
        progress_teams.visible()
    }

    override fun hideLoading() {
        progress_teams.invisible()
    }

    override fun leagueList(leagueData: List<LeagueDetail>) {
        swipeRefresh_teams.isRefreshing = false
        val item = ArrayList<StringWithTag>()

        leagueData.forEach {
            val key = it.idLeague
            val value = it.strLeague
            item.add(StringWithTag(key as String, value as String))
        }

        spinnerAdapter = ArrayAdapter(ctx, R.layout.support_simple_spinner_dropdown_item,
                item)
        spinner_teams.adapter = spinnerAdapter
        spinner_teams.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val swt = parent?.getItemAtPosition(position) as StringWithTag
                val value = swt.string
                presenter.getTeams(value)

                swipeRefresh_teams.onRefresh {
                    presenter.getTeams(value)
                }
            }
        }
    }

    override fun showSearch(searchData: List<Team>) {
        swipeRefresh_teams.isRefreshing = false
        searchTeam.clear()
        searchTeam.addAll(searchData)
        adapterSearch.notifyDataSetChanged()
    }

    override fun showTeams(teamsData: List<Team>) {
        swipeRefresh_teams.isRefreshing = false
        teams.clear()
        teams.addAll(teamsData)
        adapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.search_menu, menu)
        val searchItem: MenuItem = menu?.findItem(R.id.search_menu) as MenuItem
        val searchView: SearchView = searchItem.actionView as SearchView
        searchView.queryHint = "Telusuri..."
        searchQuery(searchView)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.search_menu -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun searchQuery(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(keyWord: String): Boolean {
                if (keyWord != "") {
                    presenter.getSearchTeams(keyWord)
                    recycler_view_teams_search.visible()
                    recycler_view_teams.gone()
                    spinner_teams.gone()
                    return true
                } else {
                    recycler_view_teams_search.gone()
                    recycler_view_teams.visible()
                    spinner_teams.visible()
                    presenter.getLeagues()
                    return true
                }
            }
        })
        adapterSearch = TeamsSearchAdapter(searchTeam) {
            startActivity<DetailTeamActivity>(
                    "teamId" to it.teamId,
                    "teamName" to it.teamName,
                    "teamBadge" to it.teamBadge,
                    "teamYear" to it.teamFormedYear,
                    "teamStadium" to it.teamStadium,
                    "teamDesc" to it.teamDescription
            )
        }
        recycler_view_teams_search.layoutManager = LinearLayoutManager(ctx)
        recycler_view_teams_search.adapter = adapterSearch
    }
}
