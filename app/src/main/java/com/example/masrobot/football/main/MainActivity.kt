package com.example.masrobot.football.main

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.example.masrobot.football.R
import com.example.masrobot.football.fragments.favorites.FavoriteFragment
import com.example.masrobot.football.fragments.matches.MatchesFragment
import com.example.masrobot.football.fragments.teams.TeamsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_matches -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, MatchesFragment(), MatchesFragment::class.java.simpleName)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_teams -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, TeamsFragment(), TeamsFragment::class.java.simpleName)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorites -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, FavoriteFragment(), FavoriteFragment::class.java.simpleName)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = R.id.navigation_matches
    }
}
