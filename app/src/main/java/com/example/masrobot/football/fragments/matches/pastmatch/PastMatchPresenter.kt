package com.example.masrobot.football.fragments.matches.pastmatch

import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.api.TheSportDBApi
import com.example.masrobot.football.model.EventResponse
import com.example.masrobot.football.model.LeagueResponse
import com.example.masrobot.football.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class PastMatchPresenter(private val view: PastMatchView,
                         private val apiRepository: ApiRepository,
                         private val gson: Gson,
                         private val context: CoroutineContextProvider = CoroutineContextProvider()) {


    fun getLeagues() {
        view.showLoading()
        async(context.main) {
            val dataLeagues = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.listAllLeague()),
                        LeagueResponse::class.java)
            }
            view.leagueList(dataLeagues.await().leagues)
            view.hideLoading()
        }
    }

    fun getPastMatch(league: String) {
        view.showLoading()
        async(context.main) {
            val dataMatch = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getPastMatch(league)),
                        EventResponse::class.java)
            }
            view.listMatch(dataMatch.await().matchSchedule)
            view.hideLoading()
        }
    }
}