package com.example.masrobot.football.fragments.matches.pastmatch


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter

import com.example.masrobot.football.R
import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.detailmatch.DetailMatchActivity
import com.example.masrobot.football.fragments.matches.pastmatch.adapter.PastMatchAdapter
import com.example.masrobot.football.model.LeagueDetail
import com.example.masrobot.football.model.MatchSchedule
import com.example.masrobot.football.utils.StringWithTag
import com.example.masrobot.football.utils.invisible
import com.example.masrobot.football.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_past_match.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity

class PastMatchFragment : Fragment(), PastMatchView {

    private lateinit var presenter: PastMatchPresenter
    private var spinnerAdapter: ArrayAdapter<StringWithTag>? = null
    private var pastMatches: MutableList<MatchSchedule> = mutableListOf()
    private lateinit var adapter: PastMatchAdapter
    private var key: String? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val request = ApiRepository()
        val gson = Gson()
        presenter = PastMatchPresenter(this, request, gson)

        // League List
        presenter.getLeagues()

        adapter = PastMatchAdapter(pastMatches) {
            startActivity<DetailMatchActivity>("matchId" to "${it.matchId}",
                    "homeTeamId" to "${it.homeTeamId}",
                    "awayTeamId" to "${it.awayTeamId}")
        }
        recycler_view_past_match.layoutManager = LinearLayoutManager(ctx)
        recycler_view_past_match.adapter = adapter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_past_match, container, false)
    }

    override fun showLoading() {
        progress_past_match.visible()
    }

    override fun hideLoading() {
        progress_past_match.invisible()
    }

    override fun leagueList(leagueData: List<LeagueDetail>) {
        swipeRefresh_past_match.isRefreshing = false

        val item = ArrayList<StringWithTag>()

        leagueData.forEach {
            val key = it.idLeague
            val value = it.strLeague
            item.add(StringWithTag(key as String, value as String))
        }

        spinnerAdapter = ArrayAdapter(ctx, R.layout.support_simple_spinner_dropdown_item,
                item)

        spinner_past_match.adapter = spinnerAdapter

        spinner_past_match.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val swt = parent?.getItemAtPosition(position) as StringWithTag
                key = swt.tag.toString()
                presenter.getPastMatch(key.toString())

                swipeRefresh_past_match.onRefresh {
                    presenter.getPastMatch(key.toString())
                }
            }
        }
    }

    override fun listMatch(matchData: List<MatchSchedule>) {
        swipeRefresh_past_match.isRefreshing = false
        pastMatches.clear()
        pastMatches.addAll(matchData)
        adapter.notifyDataSetChanged()
    }
}
