package com.example.masrobot.football.detailmatch

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.example.masrobot.football.R
import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.db.database
import com.example.masrobot.football.detailmatch.adapter.DetailMatchAdapter
import com.example.masrobot.football.model.AwayTeamDetail
import com.example.masrobot.football.model.HomeTeamDetail
import com.example.masrobot.football.model.MatchDetail
import com.example.masrobot.football.model.MatchFavorite
import com.example.masrobot.football.utils.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail_match.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.onRefresh
import java.sql.SQLClientInfoException

class DetailMatchActivity : AppCompatActivity(), DetailMatchView {

    private var matchDetailData: MutableList<MatchDetail> = mutableListOf()
    private var homeDetailData: MutableList<HomeTeamDetail> = mutableListOf()
    private var awayDetailData: MutableList<AwayTeamDetail> = mutableListOf()
    private lateinit var adapter: DetailMatchAdapter
    private lateinit var presenter: DetailMatchPresenter
    private var matchId: String = ""
    private var homeTeamId: String = ""
    private var awayTeamId: String = ""
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_match)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val intent = intent
        matchId = intent.getStringExtra("matchId")
        homeTeamId = intent.getStringExtra("homeTeamId")
        awayTeamId = intent.getStringExtra("awayTeamId")

        val request = ApiRepository()
        val gson = Gson()
        presenter = DetailMatchPresenter(this, request, gson)
        presenter.getMatchDetail(matchId, homeTeamId, awayTeamId)

        favoriteState()

        swipeRefresh_detail_match.onRefresh {
            presenter.getMatchDetail(matchId, homeTeamId, awayTeamId)
        }

        adapter = DetailMatchAdapter(matchDetailData, homeDetailData, awayDetailData)
        recycler_view_detail.layoutManager = LinearLayoutManager(this)
        recycler_view_detail.adapter = adapter
    }

    override fun showLoading() {
        progress_detail_match.visible()
    }

    override fun hideLoading() {
        progress_detail_match.invisible()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showMatchDetail(matchData: List<MatchDetail>,
                                 homeData: List<HomeTeamDetail>,
                                 awayData: List<AwayTeamDetail>) {
        swipeRefresh_detail_match.isRefreshing = false
        matchDetailData.clear()
        matchDetailData.addAll(matchData)
        homeDetailData.clear()
        homeDetailData.addAll(homeData)
        awayDetailData.clear()
        awayDetailData.addAll(awayData)
        adapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.favorite_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun favoriteState() {
        database.use {
            val result = select(MatchFavorite.TABLE_FAVORITE_MATCH)
                    .whereArgs("(MATCH_ID = {id})",
                            "id" to matchId
                    )
            val favorite = result.parseList(classParser<MatchFavorite>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    private fun addToFavorite() {
        val localFormat = toGMTFormat(matchDetailData[0].dateMatch.toString(),
                matchDetailData[0].timeMatch.toString())
        val dateLocalFormat = dateSimpleString(localFormat)
        val timeLocalFormat = timeSimpleString(localFormat)

        try {
            database.use {
                with(MatchFavorite) {
                    insert(TABLE_FAVORITE_MATCH,
                            MATCH_ID to matchDetailData[0].matchId,
                            HOME_TEAM_ID to matchDetailData[0].homeTeamId,
                            AWAY_TEAM_ID to matchDetailData[0].awayTeamId,
                            HOME_TEAM to matchDetailData[0].homeTeam,
                            AWAY_TEAM to matchDetailData[0].awayTeam,
                            HOME_SCORE to matchDetailData[0].homeScore,
                            AWAY_SCORE to matchDetailData[0].awayScore,
                            DATE_MATCH to dateLocalFormat,
                            TIME_MATCH to timeLocalFormat
                    )
                }
            }
            snackbar(swipeRefresh_detail_match, "Added to favorite").show()
        } catch (e: SQLClientInfoException) {
            snackbar(swipeRefresh_detail_match, e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(MatchFavorite.TABLE_FAVORITE_MATCH,
                        "(MATCH_ID = {id})",
                        "id" to matchId
                )
            }
            snackbar(swipeRefresh_detail_match, "Removed to favorite").show()
        } catch (e: SQLClientInfoException) {
            snackbar(swipeRefresh_detail_match, e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_favorite)
        } else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_favorite)
        }
    }
}
