package com.example.masrobot.football.detailmatch

import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.api.TheSportDBApi
import com.example.masrobot.football.model.DetailAwayTeamResponse
import com.example.masrobot.football.model.DetailEventResponse
import com.example.masrobot.football.model.DetailHomeTeamResponse
import com.example.masrobot.football.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class DetailMatchPresenter(private val view: DetailMatchView,
                           private val apiRepository: ApiRepository,
                           private val gson: Gson,
                           private val context: CoroutineContextProvider = CoroutineContextProvider()) {
    fun getMatchDetail(matchId: String, homeTeamId: String, awayTeamId: String) {
        view.showLoading()
        async(context.main) {
            val matchData = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getMatchDetail(matchId)),
                        DetailEventResponse::class.java)
            }
            val homeData = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getHomeTeam(homeTeamId)),
                        DetailHomeTeamResponse::class.java)
            }
            val awayData = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getAwayTeam(awayTeamId)),
                        DetailAwayTeamResponse::class.java)
            }
            view.showMatchDetail(matchData.await().matchDetail,
                    homeData.await().detailHomeTeam,
                    awayData.await().detailAwayTeam)
            view.hideLoading()
        }
    }
}