package com.example.masrobot.football.detailplayer

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.masrobot.football.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_player.*

class DetailPlayerActivity : AppCompatActivity() {

    private var playerBanner: String? = null
    private var playerName: String? = null
    private var playerDesc: String? = null
    private var playerPos: String? = null
    private var playerHeight: String? = null
    private var playerWeight: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_player)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val intent = intent
        playerBanner = intent.getStringExtra("playerBanner")
        playerName = intent.getStringExtra("playerName")
        playerDesc = intent.getStringExtra("playerDesc")
        playerPos = intent.getStringExtra("playerPos")
        playerHeight = intent.getStringExtra("playerHeight")
        playerWeight = intent.getStringExtra("playerWeight")

        supportActionBar?.title = playerName
        Picasso.with(this)
                .load(playerBanner)
                .placeholder(R.drawable.ic_simple_soccer_ball)
                .error(R.drawable.ic_simple_soccer_ball)
                .into(img_banner)
        tv_weight_value?.text = playerWeight?.replace("Kg","")
        tv_height_value?.text = playerHeight?.replace("m", "")
        tv_position?.text = playerPos
        tv_description?.text = playerDesc
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
