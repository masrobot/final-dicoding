package com.example.masrobot.football.searchmatch

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.widget.SearchView
import com.example.masrobot.football.R
import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.detailmatch.DetailMatchActivity
import com.example.masrobot.football.model.MatchSchedule
import com.example.masrobot.football.searchmatch.adapter.SearchMatchAdapter
import com.example.masrobot.football.utils.invisible
import com.example.masrobot.football.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_search_match.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity

class SearchMatchActivity : AppCompatActivity(), SearchMatchView {

    private lateinit var toolbar: Toolbar
    private lateinit var presenter: SearchMatchPresenter
    private var searchMatch: MutableList<MatchSchedule> = mutableListOf()
    private lateinit var adapter: SearchMatchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_match)

        toolbar = find(R.id.toolbar_search)
        setSupportActionBar(toolbar)
        with(supportActionBar) {
            this?.setDisplayHomeAsUpEnabled(true)
            this?.setDisplayShowHomeEnabled(true)
        }

        val request = ApiRepository()
        val gson = Gson()
        presenter = SearchMatchPresenter(this, request, gson)

        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(keyWord: String): Boolean {
                return false
            }

            override fun onQueryTextChange(keyWord: String): Boolean {
                presenter.searchMatch(keyWord)
                return true
            }
        })

        adapter = SearchMatchAdapter(searchMatch) {
            startActivity<DetailMatchActivity>("matchId" to "${it.matchId}",
                    "homeTeamId" to "${it.homeTeamId}",
                    "awayTeamId" to "${it.awayTeamId}")
        }
        recycler_view_search.layoutManager = LinearLayoutManager(this)
        recycler_view_search.adapter = adapter
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showLoading() {
        progress_search.visible()
    }

    override fun hideLoading() {
        progress_search.invisible()
    }

    override fun showSearchMatch(searchData: List<MatchSchedule>) {
        searchMatch.clear()
        searchMatch.addAll(searchData)
        adapter.notifyDataSetChanged()
    }
}
