package com.example.masrobot.football.fragments.matches.nextmatch

import com.example.masrobot.football.model.LeagueDetail
import com.example.masrobot.football.model.MatchSchedule

interface NextMatchView {
    fun showLoading()
    fun hideLoading()
    fun leagueList(leagueData: List<LeagueDetail>)
    fun listMatch(matchData: List<MatchSchedule>)
}