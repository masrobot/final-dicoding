package com.example.masrobot.football.fragments.player

import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.api.TheSportDBApi
import com.example.masrobot.football.model.PlayerResponse
import com.example.masrobot.football.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class PlayerPresenter(private val view: PlayerView,
                      private val apiRepository: ApiRepository,
                      private val gson: Gson,
                      private val context: CoroutineContextProvider = CoroutineContextProvider()) {
    fun getPlayer(teamName: String) {
        view.showLoading()
        async(context.main) {
            val playerData = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getPlayers(teamName)),
                        PlayerResponse::class.java)
            }
            view.showPlayerData(playerData.await().player)
            view.hideLoading()
        }
    }
}