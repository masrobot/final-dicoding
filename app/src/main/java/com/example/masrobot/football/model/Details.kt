package com.example.masrobot.football.model

import com.google.gson.annotations.SerializedName

data class LeagueDetail(
        @SerializedName("idLeague")
        var idLeague: String? = null,
        @SerializedName("strLeague")
        var strLeague: String? = null
)

data class MatchSchedule (
        @SerializedName("idEvent")
        var matchId: String? = null,
        @SerializedName("idHomeTeam")
        var homeTeamId: String? = null,
        @SerializedName("idAwayTeam")
        var awayTeamId: String? = null,
        @SerializedName("strHomeTeam")
        var homeTeam: String? = null,
        @SerializedName("strAwayTeam")
        var awayTeam: String? = null,
        @SerializedName("intHomeScore")
        var homeScore: String? = null,
        @SerializedName("intAwayScore")
        var awayScore: String? = null,
        @SerializedName("dateEvent")
        var dateMatch: String? = null,
        @SerializedName("strTime")
        var timeMatch: String? = null
)

data class MatchDetail (
        @SerializedName("idEvent")
        var matchId: String? = null,
        @SerializedName("idHomeTeam")
        var homeTeamId: String? = null,
        @SerializedName("idAwayTeam")
        var awayTeamId: String? = null,
        @SerializedName("strHomeTeam")
        var homeTeam: String? = null,
        @SerializedName("strAwayTeam")
        var awayTeam: String? = null,
        @SerializedName("intHomeScore")
        var homeScore: String? = null,
        @SerializedName("intAwayScore")
        var awayScore: String? = null,
        @SerializedName("intHomeShots")
        var homeShots: String? = null,
        @SerializedName("intAwayShots")
        var awayShots: String? = null,
        @SerializedName("dateEvent")
        var dateMatch: String? = null,
        @SerializedName("strTime")
        var timeMatch: String? = null,
        @SerializedName("strHomeGoalDetails")
        var homeGoalDetails: String? = null,
        @SerializedName("strAwayGoalDetails")
        var awayGoalDetails: String? = null,
        @SerializedName("strHomeRedCards")
        var homeRedCards: String? = null,
        @SerializedName("strAwayRedCards")
        var awayRedCards: String? = null,
        @SerializedName("strHomeYellowCards")
        var homeYellowCards: String? = null,
        @SerializedName("strAwayYellowCards")
        var awayYellowCards: String? = null,
        @SerializedName("strHomeLineupGoalkeeper")
        var homeLineupGK: String? = null,
        @SerializedName("strAwayLineupGoalkeeper")
        var awayLineupGK: String? = null,
        @SerializedName("strHomeLineupDefense")
        var homeLineupDef: String? = null,
        @SerializedName("strAwayLineupDefense")
        var awayLineupDef: String? = null,
        @SerializedName("strHomeLineupMidfield")
        var homeLineupMF: String? = null,
        @SerializedName("strAwayLineupMidfield")
        var awayLineupMF: String? = null,
        @SerializedName("strHomeLineupForward")
        var homeLineupST: String? = null,
        @SerializedName("strAwayLineupForward")
        var awayLineupST: String? = null,
        @SerializedName("strHomeLineupSubstitutes")
        var homeLineupSubs: String? = null,
        @SerializedName("strAwayLineupSubstitutes")
        var awayLineupSubs: String? = null,
        @SerializedName("strHomeFormation")
        var homeFormation: String? = null,
        @SerializedName("strAwayFormation")
        var awayFormation: String? = null
)

data class HomeTeamDetail (
        @SerializedName("strTeamBadge")
        var homeTeamBadge: String? = null
)

data class AwayTeamDetail (
        @SerializedName("strTeamBadge")
        var awayTeamBadge: String? = null
)

data class Team (
        @SerializedName("idTeam")
        var teamId: String? = null,
        @SerializedName("strTeam")
        var teamName: String? = null,
        @SerializedName("strTeamBadge")
        var teamBadge: String? = null,
        @SerializedName("strDescriptionEN")
        var teamDescription: String? = null,
        @SerializedName("intFormedYear")
        var teamFormedYear: String? = null,
        @SerializedName("strStadium")
        var teamStadium: String? = null
)

data class Player (
        @SerializedName("idPlayer")
        var playerId: String? = null,
        @SerializedName("strPlayer")
        var playerName: String? = null,
        @SerializedName("strDescriptionEN")
        var playerDesc: String? = null,
        @SerializedName("strCutout")
        var playerPic: String? = null,
        @SerializedName("strPosition")
        var playerPos: String? = null,
        @SerializedName("strFanart2")
        var playerBanner: String? = null,
        @SerializedName("strHeight")
        var playerHeight: String? = null,
        @SerializedName("strWeight")
        var playerWeight: String? = null
)

