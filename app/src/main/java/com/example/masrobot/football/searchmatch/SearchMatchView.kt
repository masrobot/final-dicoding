package com.example.masrobot.football.searchmatch

import com.example.masrobot.football.model.MatchSchedule

interface SearchMatchView {
    fun showLoading()
    fun hideLoading()
    fun showSearchMatch(searchData: List<MatchSchedule>)
}