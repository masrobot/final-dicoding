package com.example.masrobot.football.searchmatch

import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.api.TheSportDBApi
import com.example.masrobot.football.model.SearchEventResponse
import com.example.masrobot.football.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class SearchMatchPresenter(private val view: SearchMatchView,
                           private val apiRepository: ApiRepository,
                           private val gson: Gson,
                           private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun searchMatch(teamName: String) {
        view.showLoading()
        async(context.main) {
            val searchData = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.searchEvents(teamName)),
                        SearchEventResponse::class.java)
            }
            view.showSearchMatch(searchData.await().searchEvent)
            view.hideLoading()
        }
    }

}