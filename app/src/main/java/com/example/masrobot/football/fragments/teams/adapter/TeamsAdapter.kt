package com.example.masrobot.football.fragments.teams.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.football.R
import com.example.masrobot.football.model.Team
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk15.coroutines.onClick

class TeamsAdapter (private val teams: List<Team>,
                    private val listener: (Team) -> Unit) : RecyclerView.Adapter<TeamViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder =
            TeamViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_teams, parent, false))

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bindItem(teams[position], listener)
    }
}

class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view){
    private val item = view.find<CardView>(R.id.item_teams)
    private val imgBadge = view.find<ImageView>(R.id.img_team_badge)
    private val teamName = view.find<TextView>(R.id.tv_team_name)

    fun bindItem(teams: Team, listener: (Team) -> Unit) {
        item.onClick {
            listener(teams)
        }

        Picasso.with(itemView.context).load(teams.teamBadge).into(imgBadge)
        teamName.text = teams.teamName
    }
}
