package com.example.masrobot.football.model

data class MatchFavorite(val id: Long? = null,
                         val matchId: String? = null,
                         val homeTeamId: String? = null,
                         val awayTeamId: String? = null,
                         val homeTeam: String? = null,
                         val awayTeam: String? = null,
                         val homeScore: String? = null,
                         val awayScore: String? = null,
                         val dateMatch: String? = null,
                         val timeMatch: String? = null) {
    companion object {
        const val TABLE_FAVORITE_MATCH: String = "TABLE_FAVORITE_MATCH"
        const val ID: String = "ID_"
        const val MATCH_ID: String = "MATCH_ID"
        const val HOME_TEAM_ID: String = "HOME_TEAM_ID"
        const val AWAY_TEAM_ID: String = "AWAY_TEAM_ID"
        const val HOME_TEAM: String = "HOME_TEAM"
        const val AWAY_TEAM: String = "AWAY_TEAM"
        const val HOME_SCORE: String = "HOME_SCORE"
        const val AWAY_SCORE: String = "AWAY_SCORE"
        const val DATE_MATCH: String = "DATE_MATCH"
        const val TIME_MATCH: String = "TIME_MATCH"
    }
}

data class TeamFavorite(val id: Long? = null,
                        val teamId: String? = null,
                        val teamName: String? = null,
                        val teamBadge: String? = null,
                        val teamDesc: String? = null,
                        val teamStadium: String? = null,
                        val teamYear: String? = null) {
    companion object {
        const val TABLE_FAVORITE_TEAM: String = "TABLE_FAVORITE_TEAM"
        const val ID: String = "ID_"
        const val TEAM_ID: String = "TEAM_ID"
        const val TEAM_NAME: String = "TEAM_NAME"
        const val TEAM_BADGE: String = "TEAM_BADGE"
        const val TEAM_DESC: String = "TEAM_DESC"
        const val TEAM_YEAR: String = "TEAM_YEAR"
        const val TEAM_STADIUM: String = "TEAM_STADIUM"
    }
}