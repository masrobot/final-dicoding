package com.example.masrobot.football.fragments.matches.pastmatch.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.football.R
import com.example.masrobot.football.model.MatchSchedule
import com.example.masrobot.football.utils.*
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk15.coroutines.onClick

class PastMatchAdapter (private val pastMatches: List<MatchSchedule>,
                        private val listener: (MatchSchedule) -> Unit) : RecyclerView.Adapter<PastViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PastViewHolder =
            PastViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_matches, parent, false))

    override fun getItemCount(): Int = pastMatches.size

    override fun onBindViewHolder(holder: PastViewHolder, position: Int) {
        holder.bindItem(pastMatches[position], listener)
    }
}

class PastViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val item = view.find<CardView>(R.id.card_container)
    private val dateMatch = view.find<TextView>(R.id.tv_tgl_match)
    private val timeMatch = view.find<TextView>(R.id.tv_jam_match)
    private val homeTeam = view.find<TextView>(R.id.tv_team_home)
    private val awayTeam = view.find<TextView>(R.id.tv_team_away)
    private val homeScore = view.find<TextView>(R.id.tv_home_score)
    private val awayScore = view.find<TextView>(R.id.tv_away_score)
    private val createEvent = view.find<ImageView>(R.id.img_add_event)

    fun bindItem(pastMatches: MatchSchedule, listener: (MatchSchedule) -> Unit) {
        item.onClick {
            listener(pastMatches)
        }
        val localFormat = toGMTFormat(pastMatches.dateMatch.toString(), pastMatches.timeMatch.toString())
        val dateLocalFormat = dateSimpleString(localFormat)
        val timeLocalFormat = timeSimpleString(localFormat)

        dateMatch.text = dateLocalFormat
        timeMatch.text = timeLocalFormat
        homeTeam.text = pastMatches.homeTeam
        awayTeam.text = pastMatches.awayTeam
        homeScore.text = pastMatches.homeScore
        awayScore.text = pastMatches.awayScore

        createEvent.invisible()
    }
}
