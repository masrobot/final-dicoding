package com.example.masrobot.football.fragments.player


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.masrobot.football.R
import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.detailplayer.DetailPlayerActivity
import com.example.masrobot.football.fragments.player.adapter.PlayerAdapter
import com.example.masrobot.football.model.Player
import com.example.masrobot.football.utils.invisible
import com.example.masrobot.football.utils.visible
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_player.*
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

class PlayerFragment : Fragment(), PlayerView {

    var teamName: String? = null
    private lateinit var presenter: PlayerPresenter
    private var playerList: MutableList<Player> = mutableListOf()
    private lateinit var adapter: PlayerAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val request = ApiRepository()
        val gson = Gson()
        presenter = PlayerPresenter(this, request, gson)
        presenter.getPlayer(teamName.toString())

        swipeRefresh_player.onRefresh {
            presenter.getPlayer(teamName.toString())
        }

        adapter = PlayerAdapter(playerList) {
            startActivity<DetailPlayerActivity>(
                    "playerBanner" to it.playerBanner,
                    "playerName" to it.playerName,
                    "playerDesc" to it.playerDesc,
                    "playerPos" to it.playerPos,
                    "playerHeight" to it.playerHeight,
                    "playerWeight" to it.playerWeight
            )
        }
        recycler_view_player.layoutManager = LinearLayoutManager(ctx)
        recycler_view_player.adapter = adapter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_player, container, false)
    }

    override fun showLoading() {
        progress_player.visible()
    }

    override fun hideLoading() {
        progress_player.invisible()
    }

    override fun showPlayerData(playerData: List<Player>) {
        swipeRefresh_player.isRefreshing = false
        playerList.clear()
        playerList.addAll(playerData)
        adapter.notifyDataSetChanged()
    }
}
