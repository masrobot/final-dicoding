package com.example.masrobot.football.detailteam

import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.example.masrobot.football.R
import com.example.masrobot.football.db.database
import com.example.masrobot.football.fragments.overview.OverviewFragment
import com.example.masrobot.football.fragments.player.PlayerFragment
import com.example.masrobot.football.model.TeamFavorite
import com.example.masrobot.football.utils.TabsAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_team.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.find
import java.sql.SQLClientInfoException

class DetailTeamActivity : AppCompatActivity() {

    private var teamId: String? = null
    private var teamName: String? = null
    private var teamBadge: String? = null
    private var teamYear: String? = null
    private var teamStadium: String? = null
    private var teamDesc: String? = null
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_team)

        val toolbar = find<Toolbar>(R.id.toolbar_detail_team)
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val collapsingToolbarLayout = find<CollapsingToolbarLayout>(R.id.collapsing_toolbar_detail_team)
        collapsingToolbarLayout.title = ""

        val intent = intent
        teamId = intent.getStringExtra("teamId")
        teamName = intent.getStringExtra("teamName")
        teamBadge = intent.getStringExtra("teamBadge")
        teamYear = intent.getStringExtra("teamYear")
        teamStadium = intent.getStringExtra("teamStadium")
        teamDesc = intent.getStringExtra("teamDesc")

        Picasso.with(this).load(teamBadge).into(img_team_badge)
        tv_team_name?.text = teamName
        tv_team_year?.text = teamYear
        tv_team_stadium?.text = teamStadium.toString()

        val overviewFragment = OverviewFragment()
        val playerFragment = PlayerFragment()
        overviewFragment.teamDesc = teamDesc
        playerFragment.teamName = teamName

        val tabsAdapter = TabsAdapter(supportFragmentManager)
        tabsAdapter.addFragment(overviewFragment, "OVERVIEW")
        tabsAdapter.addFragment(playerFragment, "PLAYER")
        view_pager_detail_team.adapter = tabsAdapter
        tabs_detail_team.setupWithViewPager(view_pager_detail_team)

        favoriteState()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.favorite_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun favoriteState() {
        database.use {
            val result = select(TeamFavorite.TABLE_FAVORITE_TEAM)
                    .whereArgs("(TEAM_ID = {id})",
                            "id" to teamId.toString()
                    )
            val favorite = result.parseList(classParser<TeamFavorite>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    private fun addToFavorite() {
        try {
            database.use {
                with(TeamFavorite) {
                    insert(TABLE_FAVORITE_TEAM,
                            TEAM_ID to teamId,
                            TEAM_NAME to teamName,
                            TEAM_BADGE to teamBadge,
                            TEAM_DESC to teamDesc,
                            TEAM_YEAR to teamYear,
                            TEAM_STADIUM to teamStadium
                    )
                }
            }
            snackbar(view_pager_detail_team, "Added to favorite").show()
        } catch (e: SQLClientInfoException) {
            snackbar(view_pager_detail_team, e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(TeamFavorite.TABLE_FAVORITE_TEAM,
                        "(TEAM_ID = {id})",
                        "id" to teamId.toString()
                )
            }
            snackbar(view_pager_detail_team, "Removed to favorite").show()
        } catch (e: SQLClientInfoException) {
            snackbar(view_pager_detail_team, e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_favorite)
        } else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_favorite)
        }
    }
}
