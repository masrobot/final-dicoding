package com.example.masrobot.football.detailmatch

import com.example.masrobot.football.model.AwayTeamDetail
import com.example.masrobot.football.model.HomeTeamDetail
import com.example.masrobot.football.model.MatchDetail

interface DetailMatchView {
    fun showLoading()
    fun hideLoading()
    fun showMatchDetail(matchData: List<MatchDetail>, homeData: List<HomeTeamDetail>, awayData: List<AwayTeamDetail>)
}