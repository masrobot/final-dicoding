package com.example.masrobot.football.fragments.favorites.teamfavorite


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.masrobot.football.R
import com.example.masrobot.football.db.database
import com.example.masrobot.football.detailteam.DetailTeamActivity
import com.example.masrobot.football.fragments.favorites.teamfavorite.adapter.TeamFavoriteAdapter
import com.example.masrobot.football.model.TeamFavorite
import kotlinx.android.synthetic.main.fragment_team_favorite.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity

class TeamFavotiteFragment : Fragment() {

    private var teamFavoriteData: MutableList<TeamFavorite> = mutableListOf()
    private lateinit var adapter: TeamFavoriteAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = TeamFavoriteAdapter(teamFavoriteData) {
            startActivity<DetailTeamActivity>(
                    "teamId" to it.teamId,
                    "teamName" to it.teamName,
                    "teamBadge" to it.teamBadge,
                    "teamYear" to it.teamYear,
                    "teamStadium" to it.teamStadium,
                    "teamDesc" to it.teamDesc
            )
        }

        recycler_view_favorite_team.layoutManager = LinearLayoutManager(ctx)
        recycler_view_favorite_team.adapter = adapter

        showFavorite()

        swipeRefresh_favorite_team.onRefresh {
            teamFavoriteData.clear()
            showFavorite()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_team_favorite, container, false)
    }

    private fun showFavorite() {
        context?.database?.use {
            swipeRefresh_favorite_team.isRefreshing = false
            val result = select(TeamFavorite.TABLE_FAVORITE_TEAM)
            val favorite = result.parseList(classParser<TeamFavorite>())
            teamFavoriteData.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }
}
