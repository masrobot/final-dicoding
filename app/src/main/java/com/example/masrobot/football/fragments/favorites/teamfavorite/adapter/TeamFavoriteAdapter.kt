package com.example.masrobot.football.fragments.favorites.teamfavorite.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.football.R
import com.example.masrobot.football.model.TeamFavorite
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk15.coroutines.onClick

class TeamFavoriteAdapter (private val teamFavoriteData: List<TeamFavorite>,
                           private val listener: (TeamFavorite) -> Unit) : RecyclerView.Adapter<TeamViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder =
            TeamViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_teams, parent, false))

    override fun getItemCount(): Int = teamFavoriteData.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bindItem(teamFavoriteData[position], listener)
    }
}

class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view){
    private val item = view.find<CardView>(R.id.item_teams)
    private val imgBadge = view.find<ImageView>(R.id.img_team_badge)
    private val teamName = view.find<TextView>(R.id.tv_team_name)

    fun bindItem(teamFavoriteData: TeamFavorite, listener: (TeamFavorite) -> Unit) {
        item.onClick {
            listener(teamFavoriteData)
        }

        Picasso.with(itemView.context).load(teamFavoriteData.teamBadge).into(imgBadge)
        teamName.text = teamFavoriteData.teamName
    }
}