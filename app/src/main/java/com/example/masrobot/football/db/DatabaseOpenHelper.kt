package com.example.masrobot.football.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.masrobot.football.model.MatchFavorite
import com.example.masrobot.football.model.Team
import com.example.masrobot.football.model.TeamFavorite
import org.jetbrains.anko.db.*

class DatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx,
        "Favorite.db",
        null, 1) {
    companion object {
        private var instance: DatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DatabaseOpenHelper {
            if (instance == null) {
                instance = DatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as DatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(MatchFavorite.TABLE_FAVORITE_MATCH, true,
                MatchFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                MatchFavorite.MATCH_ID to TEXT + UNIQUE,
                MatchFavorite.HOME_TEAM_ID to TEXT,
                MatchFavorite.AWAY_TEAM_ID to TEXT,
                MatchFavorite.HOME_TEAM to TEXT,
                MatchFavorite.AWAY_TEAM to TEXT,
                MatchFavorite.HOME_SCORE to TEXT,
                MatchFavorite.AWAY_SCORE to TEXT,
                MatchFavorite.DATE_MATCH to TEXT,
                MatchFavorite.TIME_MATCH to TEXT
        )
        db?.createTable(TeamFavorite.TABLE_FAVORITE_TEAM, true,
                TeamFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                TeamFavorite.TEAM_ID to TEXT + UNIQUE,
                TeamFavorite.TEAM_NAME to TEXT,
                TeamFavorite.TEAM_BADGE to TEXT,
                TeamFavorite.TEAM_DESC to TEXT,
                TeamFavorite.TEAM_YEAR to TEXT,
                TeamFavorite.TEAM_STADIUM to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(MatchFavorite.TABLE_FAVORITE_MATCH, true)
        db?.dropTable(TeamFavorite.TABLE_FAVORITE_TEAM, true)
    }
}

val Context.database: DatabaseOpenHelper
    get() = DatabaseOpenHelper.getInstance(applicationContext)