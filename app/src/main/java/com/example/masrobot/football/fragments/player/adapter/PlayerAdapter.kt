package com.example.masrobot.football.fragments.player.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.football.R
import com.example.masrobot.football.model.Player
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk15.coroutines.onClick

class PlayerAdapter (private val player: List<Player>,
                     private val listener: (Player) -> Unit) : RecyclerView.Adapter<PlayerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder =
            PlayerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_players, parent, false))

    override fun getItemCount(): Int = player.size

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) {
        holder.bindItem(player[position], listener)
    }
}

class PlayerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val item = view.find<CardView>(R.id.item_player)
    private val imgPlayer = view.find<ImageView>(R.id.img_profil_pic)
    private val namePlayer = view.find<TextView>(R.id.tv_player_name)
    private val positionPlayer = view.find<TextView>(R.id.tv_player_pos)

    fun bindItem(player: Player, listener: (Player) -> Unit) {
        item.onClick {
            listener(player)
        }

        Picasso.with(itemView.context).load(player.playerPic).into(imgPlayer)
        namePlayer.text = player.playerName
        positionPlayer.text = player.playerPos
    }
}
