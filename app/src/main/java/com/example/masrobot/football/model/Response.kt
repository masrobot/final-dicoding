package com.example.masrobot.football.model

import com.google.gson.annotations.SerializedName

data class LeagueResponse (
        @SerializedName("countrys")
        val leagues: List<LeagueDetail>
)

data class EventResponse(
        @SerializedName("events")
        val matchSchedule: MutableList<MatchSchedule>
)

data class SearchEventResponse(
        @SerializedName("event")
        val searchEvent: List<MatchSchedule>
)

data class DetailEventResponse(
        @SerializedName("events")
        val matchDetail: List<MatchDetail>
)

data class DetailHomeTeamResponse(
        @SerializedName("teams")
        val detailHomeTeam: List<HomeTeamDetail>
)

data class DetailAwayTeamResponse(
        @SerializedName("teams")
        val detailAwayTeam: List<AwayTeamDetail>
)

data class TeamResponse(
        @SerializedName("teams")
        val teams: List<Team>
)

data class PlayerResponse (
        @SerializedName("player")
        val player: List<Player>
)
