package com.example.masrobot.football.fragments.matches.nextmatch.adapter

import android.content.Intent
import android.provider.CalendarContract
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.football.R
import com.example.masrobot.football.model.MatchSchedule
import com.example.masrobot.football.utils.*
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk15.coroutines.onClick
import java.util.*

class NextMatchAdapter (private val nextMatches: List<MatchSchedule>,
                        private val listener: (MatchSchedule) -> Unit) : RecyclerView.Adapter<NextViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NextViewHolder =
            NextViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_matches, parent, false))

    override fun getItemCount(): Int = nextMatches.size

    override fun onBindViewHolder(holder: NextViewHolder, position: Int) {
        holder.bindItem(nextMatches[position], listener)
    }
}

class NextViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val item = view.find<CardView>(R.id.card_container)
    private val dateMatch = view.find<TextView>(R.id.tv_tgl_match)
    private val timeMatch = view.find<TextView>(R.id.tv_jam_match)
    private val homeTeam = view.find<TextView>(R.id.tv_team_home)
    private val awayTeam = view.find<TextView>(R.id.tv_team_away)
    private val homeScore = view.find<TextView>(R.id.tv_home_score)
    private val awayScore = view.find<TextView>(R.id.tv_away_score)
    private val eventButton = view.find<ImageView>(R.id.img_add_event)

    fun bindItem(nextMatches: MatchSchedule, listener: (MatchSchedule) -> Unit) {
        item.onClick {
            listener(nextMatches)
        }

        val localFormat = toGMTFormat(nextMatches.dateMatch.toString(), nextMatches.timeMatch.toString())
        val dateLocalFormat = dateSimpleString(localFormat)
        val timeLocalFormat = timeSimpleString(localFormat)

        dateMatch.text = dateLocalFormat
        timeMatch.text = timeLocalFormat
        homeTeam.text = nextMatches.homeTeam
        awayTeam.text = nextMatches.awayTeam

        homeScore.gone()
        awayScore.gone()

        eventButton.onClick {
            val calID: Long = 3
            val beginTime = Calendar.getInstance()
            beginTime.set(year(nextMatches.dateMatch) as Int,
                    month(nextMatches.dateMatch) as Int,
                    day(nextMatches.dateMatch) as Int,
                    hour(nextMatches.timeMatch) as Int,
                    minute(nextMatches.timeMatch) as Int)
            val startMillis: Long = beginTime.timeInMillis
            val endTime = Calendar.getInstance()
            endTime.set(year(nextMatches.dateMatch) as Int,
                    month(nextMatches.dateMatch) as Int,
                    day(nextMatches.dateMatch) as Int,
                    hour(nextMatches.timeMatch) as Int + 3,
                    minute(nextMatches.timeMatch) as Int)
            val endMillis: Long = endTime.timeInMillis
            val intent = Intent(Intent.ACTION_INSERT)
            intent.type = "vnd.android.cursor.item/event"
            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
            intent.putExtra(CalendarContract.Events.TITLE,
                    "${nextMatches.homeTeam} vs ${nextMatches.awayTeam}")
            intent.putExtra(CalendarContract.Events.DESCRIPTION,
                    "Match : ${nextMatches.homeTeam} vs ${nextMatches.awayTeam}")
            intent.putExtra(CalendarContract.Events.CALENDAR_ID, calID)
            intent.putExtra(CalendarContract.Events.EVENT_TIMEZONE, "Asia/Jakarta")
            itemView.context.startActivity(intent)
        }
    }
}
