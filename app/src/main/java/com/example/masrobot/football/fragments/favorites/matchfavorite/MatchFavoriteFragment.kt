package com.example.masrobot.football.fragments.favorites.matchfavorite


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.masrobot.football.R
import com.example.masrobot.football.db.database
import com.example.masrobot.football.detailmatch.DetailMatchActivity
import com.example.masrobot.football.detailmatch.adapter.DetailMatchAdapter
import com.example.masrobot.football.fragments.favorites.matchfavorite.adapter.MatchFavoriteAdapter
import com.example.masrobot.football.model.MatchFavorite
import kotlinx.android.synthetic.main.fragment_match_favorite.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity


class MatchFavoriteFragment : Fragment() {

    private var favoriteMatchData: MutableList<MatchFavorite> = mutableListOf()
    private lateinit var adapter: MatchFavoriteAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = MatchFavoriteAdapter(favoriteMatchData) {
            startActivity<DetailMatchActivity>(
                    "matchId" to it.matchId,
                    "homeTeamId" to it.homeTeamId,
                    "awayTeamId" to it.awayTeamId
            )
        }

        recycler_view_favorite_match.layoutManager = LinearLayoutManager(ctx)
        recycler_view_favorite_match.adapter = adapter

        showFavorite()

        swipeRefresh_favorite_match.onRefresh {
            favoriteMatchData.clear()
            showFavorite()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_match_favorite, container, false)
    }

    private fun showFavorite() {
        context?.database?.use {
            swipeRefresh_favorite_match.isRefreshing = false
            val result = select(MatchFavorite.TABLE_FAVORITE_MATCH)
            val favorite = result.parseList(classParser<MatchFavorite>())
            favoriteMatchData.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }
}
