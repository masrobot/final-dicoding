package com.example.masrobot.football.searchmatch.adapter

import android.content.Intent
import android.provider.CalendarContract
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.football.R
import com.example.masrobot.football.model.MatchSchedule
import com.example.masrobot.football.utils.*
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk15.coroutines.onClick
import java.util.*

class SearchMatchAdapter (private val searchMatches: List<MatchSchedule>,
                          private val listener: (MatchSchedule) -> Unit) : RecyclerView.Adapter<SearchViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder =
            SearchViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_matches, parent, false))

    override fun getItemCount(): Int = searchMatches.size

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bindItem(searchMatches[position], listener)
    }
}

class SearchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val item = view.find<CardView>(R.id.card_container)
    private val dateMatch = view.find<TextView>(R.id.tv_tgl_match)
    private val timeMatch = view.find<TextView>(R.id.tv_jam_match)
    private val homeTeam = view.find<TextView>(R.id.tv_team_home)
    private val awayTeam = view.find<TextView>(R.id.tv_team_away)
    private val homeScore = view.find<TextView>(R.id.tv_home_score)
    private val awayScore = view.find<TextView>(R.id.tv_away_score)
    private val createEvent = view.find<ImageView>(R.id.img_add_event)

    fun bindItem(searchMatches: MatchSchedule, listener: (MatchSchedule) -> Unit) {
        item.onClick {
            listener(searchMatches)
        }

        if (searchMatches.homeScore != null && searchMatches.awayScore != null) {
            createEvent.invisible()
        } else {
            createEvent.visible()
        }

        val localFormat = toGMTFormat(searchMatches.dateMatch.toString(), searchMatches.timeMatch.toString())
        val dateLocalFormat = dateSimpleString(localFormat)
        val timeLocalFormat = timeSimpleString(localFormat)

        dateMatch.text = dateLocalFormat
        if (searchMatches.timeMatch != null) {
            timeMatch.text = timeLocalFormat
        } else {
            timeMatch.invisible()
        }
        homeTeam.text = searchMatches.homeTeam
        awayTeam.text = searchMatches.awayTeam
        homeScore.text = searchMatches.homeScore
        awayScore.text = searchMatches.awayScore

        if (searchMatches.timeMatch != null) {
            createEvent.onClick {
                val calID: Long = 3
                val beginTime = Calendar.getInstance()
                beginTime.set(year(searchMatches.dateMatch) as Int,
                        month(searchMatches.dateMatch) as Int,
                        day(searchMatches.dateMatch) as Int,
                        hour(searchMatches.timeMatch) as Int,
                        minute(searchMatches.timeMatch) as Int)
                val startMillis: Long = beginTime.timeInMillis
                val endTime = Calendar.getInstance()
                endTime.set(year(searchMatches.dateMatch) as Int,
                        month(searchMatches.dateMatch) as Int,
                        day(searchMatches.dateMatch) as Int,
                        hour(searchMatches.timeMatch) as Int + 3,
                        minute(searchMatches.timeMatch) as Int)
                val endMillis: Long = endTime.timeInMillis
                val intent = Intent(Intent.ACTION_INSERT)
                intent.type = "vnd.android.cursor.item/event"
                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
                intent.putExtra(CalendarContract.Events.TITLE,
                        "${searchMatches.homeTeam} vs ${searchMatches.awayTeam}")
                intent.putExtra(CalendarContract.Events.DESCRIPTION,
                        "Match : ${searchMatches.homeTeam} vs ${searchMatches.awayTeam}")
                intent.putExtra(CalendarContract.Events.CALENDAR_ID, calID)
                intent.putExtra(CalendarContract.Events.EVENT_TIMEZONE, "Asia/Jakarta")
                itemView.context.startActivity(intent)
            }
        } else {
            createEvent.invisible()
        }
    }
}