package com.example.masrobot.football.fragments.player

import com.example.masrobot.football.model.Player

interface PlayerView {
    fun showLoading()
    fun hideLoading()
    fun showPlayerData(playerData: List<Player>)
}