package com.example.masrobot.football.fragments.favorites.matchfavorite.adapter

import android.content.Intent
import android.provider.CalendarContract
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.football.R
import com.example.masrobot.football.model.MatchFavorite
import com.example.masrobot.football.utils.*
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk15.coroutines.onClick
import java.util.*

class MatchFavoriteAdapter (private val detailMatch: List<MatchFavorite>,
                            private val listener: (MatchFavorite) -> Unit) :
        RecyclerView.Adapter<MatchFavoriteViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchFavoriteViewHolder =
            MatchFavoriteViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_matches, parent, false))

    override fun getItemCount(): Int = detailMatch.size

    override fun onBindViewHolder(holder: MatchFavoriteViewHolder, position: Int) {
        holder.bindItem(detailMatch[position], listener)
    }
}

class MatchFavoriteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val item = view.find<CardView>(R.id.card_container)
    private val dateMatch = view.find<TextView>(R.id.tv_tgl_match)
    private val timeMatch = view.find<TextView>(R.id.tv_jam_match)
    private val homeTeam = view.find<TextView>(R.id.tv_team_home)
    private val awayTeam = view.find<TextView>(R.id.tv_team_away)
    private val homeScore = view.find<TextView>(R.id.tv_home_score)
    private val awayScore = view.find<TextView>(R.id.tv_away_score)
    private val eventButton = view.find<ImageView>(R.id.img_add_event)

    fun bindItem(detailMatch: MatchFavorite, listener: (MatchFavorite) -> Unit) {
        item.onClick {
            listener(detailMatch)
        }

        dateMatch.text = detailMatch.dateMatch
        timeMatch.text = detailMatch.timeMatch
        homeTeam.text = detailMatch.homeTeam
        awayTeam.text = detailMatch.awayTeam
        homeScore.text = detailMatch.homeScore
        awayScore.text = detailMatch.awayScore

        eventButton.invisible()
    }
}
