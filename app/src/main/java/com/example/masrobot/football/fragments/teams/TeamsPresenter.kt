package com.example.masrobot.football.fragments.teams

import com.example.masrobot.football.api.ApiRepository
import com.example.masrobot.football.api.TheSportDBApi
import com.example.masrobot.football.model.LeagueResponse
import com.example.masrobot.football.model.TeamResponse
import com.example.masrobot.football.utils.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class TeamsPresenter(private val view: TeamsView,
                     private val apiRepository: ApiRepository,
                     private val gson: Gson,
                     private val context: CoroutineContextProvider = CoroutineContextProvider()) {
    fun getLeagues() {
        view.showLoading()
        async(context.main) {
            val dataLeagues = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.listAllLeague()),
                        LeagueResponse::class.java)
            }
            view.leagueList(dataLeagues.await().leagues)
            view.hideLoading()
        }
    }

    fun getSearchTeams(teamName: String) {
        view.showLoading()
        async(context.main) {
            val dataSearch = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.searchTeams(teamName)),
                        TeamResponse::class.java)
            }
            view.showSearch(dataSearch.await().teams)
            view.hideLoading()
        }
    }

    fun getTeams(league: String) {
        view.showLoading()
        async(context.main) {
            val dataTeams = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeams(league)),
                        TeamResponse::class.java)
            }
            view.showTeams(dataTeams.await().teams)
            view.hideLoading()
        }
    }
}