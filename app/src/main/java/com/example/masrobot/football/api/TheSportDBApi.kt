package com.example.masrobot.football.api

import com.example.masrobot.football.BuildConfig

object TheSportDBApi {
    const val baseURL = "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}"
    fun listAllLeague(): String = "$baseURL/search_all_leagues.php?s=Soccer"
    fun getPastMatch(league: String): String = "$baseURL/eventspastleague.php?id=$league"
    fun getNextMatch(league: String): String = "$baseURL/eventsnextleague.php?id=$league"
    fun searchTeams(teamName: String): String = "$baseURL/searchteams.php?t=$teamName"
    fun searchEvents(teamName: String): String = "$baseURL/searchevents.php?e=$teamName"
    fun getMatchDetail(matchId: String?): String = "$baseURL/lookupevent.php?id=$matchId"
    fun getHomeTeam(homeTeamId: String?): String = "$baseURL/lookupteam.php?id=$homeTeamId"
    fun getAwayTeam(awayTeamId: String?): String = "$baseURL/lookupteam.php?id=$awayTeamId"
    fun getTeams(league: String): String = "$baseURL/search_all_teams.php?l=$league"
    fun getPlayers(teamName: String): String = "$baseURL/searchplayers.php?t=$teamName"
}