package com.example.masrobot.football.utils

import android.annotation.SuppressLint
import android.view.View
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

@SuppressLint("SimpleDateFormat")
fun toGMTFormat(date: String, time: String): Date? {
    val formatStrings = ArrayList<SimpleDateFormat>()
    formatStrings.add(SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
    formatStrings.add(SimpleDateFormat("yyyy-MM-dd HH:mm:ssXXX"))
    for (i in formatStrings) {
        i.timeZone = TimeZone.getTimeZone("UTC")
        val dateTime = "$date $time"
        return i.parse(dateTime)
    }
    return null
}

@SuppressLint("SimpleDateFormat")
fun dateSimpleString(date: Date?): String? = with(date ?: Date()) {
    SimpleDateFormat("EEE, dd MMM yyy").format(this)
}

@SuppressLint("SimpleDateFormat")
fun timeSimpleString(date: Date?): String? = with(date ?: Date()) {
    SimpleDateFormat("HH:mm").format(this)
}

// Separate time
@SuppressLint("SimpleDateFormat")
fun year(date: String?): Int? {
    val dateString = "$date"
    val dateParse = SimpleDateFormat("yyyy-MM-dd")
    val dateFormat = dateParse.parse(dateString)
    val formatter = SimpleDateFormat("yyyy")
    return formatter.format(dateFormat).toInt()
}

@SuppressLint("SimpleDateFormat")
fun month(date: String?): Int? {
    val dateString = "$date"
    val dateParse = SimpleDateFormat("yyyy-MM-dd")
    val dateFormat = dateParse.parse(dateString)
    val formatter = SimpleDateFormat("MM")
    return formatter.format(dateFormat).toInt()-1
}

@SuppressLint("SimpleDateFormat")
fun day(date: String?): Int? {
    val dateString = "$date"
    val dateParse = SimpleDateFormat("yyyy-MM-dd")
    val dateFormat = dateParse.parse(dateString)
    val formatter = SimpleDateFormat("dd")
    return formatter.format(dateFormat).toInt()
}

@SuppressLint("SimpleDateFormat")
fun hour(time: String?): Int? {
    val timeString = "$time"
    val formatStrings = ArrayList<SimpleDateFormat>()
    formatStrings.add(SimpleDateFormat("HH:mm:ss"))
    formatStrings.add(SimpleDateFormat("HH:mm:ssXXX"))
    for (i in formatStrings) {
        i.timeZone = TimeZone.getTimeZone("Asia/Jakarta")
        val timeFormat = i.parse(timeString)
        val formatter = SimpleDateFormat("HH")
        return formatter.format(timeFormat).toInt()
    }
    return null
}

@SuppressLint("SimpleDateFormat")
fun minute(time: String?): Int? {
    val timeString = "$time"
    val formatStrings = ArrayList<SimpleDateFormat>()
    formatStrings.add(SimpleDateFormat("HH:mm:ss"))
    formatStrings.add(SimpleDateFormat("HH:mm:ssXXX"))
    for (i in formatStrings) {
        i.timeZone = TimeZone.getTimeZone("Asia/Jakarta")
        val timeFormat = i.parse(timeString)
        val formatter = SimpleDateFormat("mm")
        return formatter.format(timeFormat).toInt()
    }
    return null
}